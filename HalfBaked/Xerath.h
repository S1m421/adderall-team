#pragma once
#include "SDKEX_Headers.h"
#include <random>
#include "Prediction.h"
#include "TargetSelector.h"
#include <thread>
#include <mutex>

/*
Pending for 20/12/2018

Structure (PRIORITY FOR LOLHERO):

1.- Buff Check
2.- Enemy States Check
3.- NonPlacebo Fucking Circle Pred
4.- Spell Q: (751 <-> 1400 Range) Manager
5.- NearMouse Radius
6.- Localplayer & Target AA animation check (Adderall AIO isAfterAA for localplayer example)

Features:

1.- Spell Drawings
2.- Menu Options
3.- Geo Drawings
4.- Spell Q: Fast and Slow logic

(Most of features are done already but not finished due to lack of Structure)

8 Steps for SIMPLE(&efficient)XERATH V1 ETA 21/12
*/
SDKCOLOR ColorRed = { 255, 0, 255, 255 };
SDKVECTOR DirectionVector = { 0, 0, 1.f };

//global TS targets
AttackableUnit targetQ750;
AttackableUnit targetE;
AttackableUnit targetEs;
AttackableUnit targetR;

namespace Xearth {
	SDKVECTOR emptyVec = { 0, 0, 0 };
	const char* BuffName;

	void Execute() { // executes in main gameScene

		int Qstate;
		int Wstate;
		int Estate;
		int Rstate;

		SDK_SPELL Q;
		SDK_SPELL W;
		SDK_SPELL E;
		SDK_SPELL R;
		SdkGetAISpell(g_LocalPlayer, 0, &Q);
		SdkGetAISpell(g_LocalPlayer, 1, &W);
		SdkGetAISpell(g_LocalPlayer, 2, &E);
		SdkGetAISpell(g_LocalPlayer, 3, &R);

		// INIT selected override
		LTargetSelector().setSelectedTarget();

		// R Logic
		if (GetAsyncKeyState(ultiGlobal))
		{
			if (lClick && selectedTarget.Object != NULL) {
				targetR = selectedTarget;
			}
			else {
				targetR = LTargetSelector().returnTarget(TSmode(TypeSelection), (R.CastRange));
			}

			SdkCanAICastSpell(g_LocalPlayer, 3, NULL, &Rstate);

			if (targetR.Object != NULL && targetR.isVisible() && targetR.isEnemy() && targetR.isAlive())
			{
				// Your trash ported circular ported pred is literally placebo
				//auto XerathR = Circular(&targetR, (R.CastDelay * 1.5), (R.CastRange * 0.97), (R.PrimaryCastRadius * 0.8));
				auto XerathR2 = Linear(&targetR, (R.CastDelay * 2.93), (R.MissileSpeed * 0.4), (R.CastRange * 0.97), 80.f);

				if (Rstate == SPELL_CAN_CAST_OK)
					if (SDKVECTORMATH(XerathR2) != SDKVECTORMATH(0, 0, 0))
						SdkCastSpellLocalPlayer(NULL, &XerathR2, 3, 0);
			}
		}

		// Trash Long Q Logic
		// Nothing

		// g_ofShores Combat Logic
		if (GetAsyncKeyState(comboGlobal)) {

			// TS setup
			if (lClick && selectedTarget.Object != NULL) {
				targetW = selectedTarget;
				targetQ750 = selectedTarget;
				targetEs = selectedTarget;
				targetE = selectedTarget;
			}
			else {
				targetW = LTargetSelector().returnTarget(TSmode(TypeSelection), (W.CastRange * 0.95));
				targetQ750 = LTargetSelector().returnTarget(TSmode(TypeSelection), (748.f));
				targetEs = LTargetSelector().returnTarget(TSmode(TypeSelection), (E.CastRange * 0.7));
				targetE = LTargetSelector().returnTarget(TSmode(TypeSelection), (E.CastRange * 0.95));
			}

			SdkCanAICastSpell(g_LocalPlayer, 0, NULL, &Qstate);
			SdkCanAICastSpell(g_LocalPlayer, 1, NULL, &Wstate);
			SdkCanAICastSpell(g_LocalPlayer, 2, NULL, &Estate);

			// W spell
			if (targetW.Object != NULL && targetW.isVisible() && targetW.isEnemy() && targetW.isAlive()) {

				//Debug -- Begin
				auto TestW = Circular(&targetW, (W.CastDelay * 3), 4000.f, (W.PrimaryCastRadius * 0.6));
				if (SDKVECTORMATH(TestW) != SDKVECTORMATH(0, 0, 0))
					SdkDrawCircle(&TestW, 250, &ColorRed, 1, &DirectionVector);

				//SdkUiConsoleWrite("%f", targetW.GetMovementSpeed());
				//Debug -- End

				//auto XerathW = Circular(&targetW, (W.CastDelay * 1.5), (W.CastRange * 0.97), (W.PrimaryCastRadius * 0.8));
				auto XerathW2 = Linear(&targetW, (W.CastDelay * 1.13), (W.MissileSpeed * 1.05), (W.CastRange * 0.94), 100);

				if (Wstate == SPELL_CAN_CAST_OK) {
					if (SDKVECTORMATH(XerathW2) != SDKVECTORMATH(0, 0, 0)) {
						SdkCastSpellLocalPlayer(NULL, &XerathW2, 1, 0);
					}
				}
			}

			// Q Spell
			if (targetQ750.Object != NULL && targetQ750.isVisible() && targetQ750.isEnemy() && targetQ750.isAlive()) {

				auto XerathQ750 = Linear(&targetW, Q.CastDelay, (Q.MissileSpeed * 1.25), 750.f, (W.LineWidth * 0.9));

				if (Qstate == SPELL_CAN_CAST_OK) {
					if (SDKVECTORMATH(XerathQ750) != SDKVECTORMATH(0, 0, 0))
						SdkCastSpellLocalPlayer(NULL, &XerathQ750, 0, 0);
				}
			}

			// E Spell -- Safe
			if (targetEs.Object != NULL && targetEs.isVisible() && targetEs.isEnemy() && targetEs.isAlive()) {

				if ((Wstate != SPELL_CAN_CAST_OK) || ((targetE.GetMovementSpeed() < 300.f) && (targetE.GetMovementSpeed() > 1.f)))
				{
					auto XerathESafe = Linear(&targetEs, E.CastDelay, (E.MissileSpeed * 1.25), 750.f, (E.LineWidth * 0.9));

					if (Estate == SPELL_CAN_CAST_OK) {
						if (SDKVECTORMATH(XerathESafe) != SDKVECTORMATH(0, 0, 0))
							if (!Collision(XerathESafe, E.LineWidth))
								SdkCastSpellLocalPlayer(NULL, &XerathESafe, 2, 0);
					}
				}
			}

			// E Spell -- Offensive
			/*
			if (targetE.Object != NULL && targetE.isVisible() && targetE.isEnemy() && targetE.isAlive()) {

				if ((Wstate != SPELL_CAN_CAST_OK))
				{
					auto XerathQ750 = Linear(&targetW, Q.CastDelay, (Q.MissileSpeed * 1.25), 750.f, (W.LineWidth * 0.9));

					if (Estate == SPELL_CAN_CAST_OK) {
						if (SDKVECTORMATH(XerathQ750) != SDKVECTORMATH(0, 0, 0))
							SdkCastSpellLocalPlayer(NULL, &XerathQ750, 0, 0);
					}
				}
			}
			*/

		}
	}
};