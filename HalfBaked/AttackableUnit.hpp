#pragma once
class AttackableUnit : public GameObject
{
public:
	AttackableUnit()
	{
	};
	AttackableUnit(void* obj) : GameObject(obj) {
	};
	~AttackableUnit()
	{
	};

	MAKE_RAW(isVisible, bool, SdkIsUnitVisible)
	MAKE_RAW(isMouseOver,bool, SdkGetUnitIsMouseOver)
	MAKE_GET(DeathTime,float, SdkGetUnitDeathTime)
	MAKE_GET(Health, SDK_HEALTH, SdkGetUnitHealth)
	MAKE_GET(MovementSpeed, float, SdkGetAIMovementSpeed)


	SDK_ABILITY_RESOURCE GetAbilitySlots(unsigned char index)
	{
		SDK_ABILITY_RESOURCE tmp1;
		CHECKFAIL(SdkGetUnitAbilityResource(Object, index, &tmp1));
		return tmp1;
	}
	
	struct NavData {
	public:
		SDKVECTOR Velocity;
		size_t NextWaypoint;
		PSDKVECTOR Waypoints;
		size_t NumberOfWaypoints;
	};

	NavData navData;
	void* Pointer;


	NavData getNav() {
		SdkGetAINavData(this->Pointer, NULL, NULL, &this->navData.NextWaypoint, &this->navData.NumberOfWaypoints, &this->navData.Waypoints, &this->navData.Velocity, NULL, NULL, NULL);
		return this->navData;
	};

	struct BuffInstance
	{
		const char* Name;
		unsigned char Type;
		float StartTime;
		float EndTime;
		int Stacks;
		int Count;

		// TODO: CHECKTHAT SHIIIIIIIIIIIT REXY
		bool IsValid()
		{
			float time;
			SdkGetGameTime(&time);
			return time < EndTime;
		}
	};

private:


};