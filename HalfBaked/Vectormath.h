#pragma once

#include <d3d9.h>
#include <d3dx9.h>
#include "MainSDK.h"

class SDKVECTORMATH : public D3DXVECTOR3 {
public:

	SDKVECTORMATH() {
		x = 0;
		y = 0;
		z = 0;
	}

	SDKVECTORMATH(D3DXVECTOR3 vec) {
		x = vec.x;
		y = vec.y;
		z = vec.z;
	}

	SDKVECTORMATH(float X, float Z) {
		x = X;
		y = 0;
		z = Z;
	}

	SDKVECTORMATH(float X, float Y, float Z) {
		x = X;
		y = Y;
		z = Z;
	}

	SDKVECTORMATH(SDKVECTOR vec) {
		x = vec.x;
		y = vec.y;
		z = vec.z;
	}

	SDKVECTOR toSdk() {
		return SDKVECTOR{ x,y,z };
	}

	SDKVECTORMATH To2D() {
		return SDKVECTORMATH(x, z, 0);
	}

	float Length() {
		return (float)sqrt((x * x) + (y * y) + (z * z));
	}

	float Distance(SDKVECTORMATH b) {
		SDKVECTORMATH vector = SDKVECTORMATH(x - b.x, y - b.y, z - b.z);
		return sqrtf((vector.x * vector.x) + (vector.y * vector.y) + (vector.z * vector.z));
	}

	D3DXVECTOR3 Normalize() {
		float length = Length();
		if (length != 0) {
			float inv = 1.0f / length;
			x *= inv;
			y *= inv;
			z *= inv;
		}
		return D3DXVECTOR3(x, y, z);
	}

	SDKVECTORMATH Perpendicular() {
		return SDKVECTORMATH(z, y, -x);
	}

	SDKVECTORMATH VectorNormalize() {
		float len = Length();
		*this /= len;
		return *this;
	}

	SDKVECTORMATH Extend(SDKVECTORMATH other, float Len) {
		SDKVECTORMATH n = other - *this;
		n.Normalize();
		SDKVECTORMATH C = *this + n * Len;
		return C;
	}
};