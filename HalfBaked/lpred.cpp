#include "stdafx.h"
#include <unordered_map>
#include "Globalsettings.h"
#include "Settings.h"
#include "Kaisa.h"
#include "Xerath.h"

PSDK_CONTEXT SDK_CONTEXT_GLOBAL;

// Colors in the BGRA format.
SDKCOLOR _g_ColorWhite = { 255, 255, 255, 255 };
SDKCOLOR _g_ColorYellow = { 0, 255, 255, 255 };
SDKCOLOR _g_ColorGreen = { 0, 255, 0, 255 };
SDKCOLOR _g_ColorRed = { 0, 0, 255, 255 };
SDKCOLOR _g_ColorPurple = { 128, 0, 128, 255 };

// GLOBAL MENU OPTIONS - check Globalsettings.h# 
bool bPred;
bool lClick;
static int TypeSelection = 0;


int comboGlobal = 32;
int ultiGlobal = 84;
int keyFound;

std::string charComboGlobal = std::to_string(comboGlobal);
std::string charUltiGlobal = std::to_string(ultiGlobal);
std::string valueFound = "";
std::string ultiFound = "";

const char* ComboLabel = charComboGlobal.c_str();
const char* UltiLabel = charUltiGlobal.c_str();

static bool ComboClicked = false;
static bool UltiClicked = false;
static bool keyPressed = false;
static bool ComboListenState = false;
static bool UltiListenState = false;

 BOOL WINAPI DllMain(_In_ HINSTANCE hinstDLL, _In_ DWORD fdwReason, _In_ LPVOID lpvReserved) {
	 UNREFERENCED_PARAMETER(hinstDLL);

	 if (fdwReason == DLL_PROCESS_ATTACH)
	 {
		 SDK_EXTRACT_CONTEXT(lpvReserved);
		 if (!SDK_CONTEXT_GLOBAL)
			 return FALSE;

		 if (!SDKSTATUS_SUCCESS(SdkNotifyLoadedModule("AdderallAIO", SDK_VERSION)))
			 return FALSE;

		 SdkRegisterOnLoad([](void* UserData) -> void
		 {
			 UNREFERENCED_PARAMETER(UserData);					 					 

			 SdkRegisterOverlayScene(DrawOverlayScene, NULL);
			 SdkRegisterGameScene(DrawGameScene, NULL);			 

			 EntityManager::Initialize();			 
			 
			 loadSettings();			

		 }, NULL);
	 }
	 else if (fdwReason == DLL_PROCESS_DETACH)
	 {
	 }
	 return TRUE;
 }

void __cdecl DrawOverlayScene(_In_ void* UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	// Local menu vars
	bool ObjectExpanded = false;
	bool TSExpanded = false;
	bool keySettingsExpand = false;
	bool TSclick = false;
	bool closeDistance = false;	
	bool lowHP = false;
	bool itemClicked = false;	
	bool comboChange = NULL;
	bool lClicked = NULL;	
	
	// Key Mappings
	std::unordered_map<int, std::string> keyFinder;
	keyFinder.emplace(VK_LBUTTON, "Left_Mouse");
	keyFinder.emplace(VK_RBUTTON, "Right_Mouse");
	keyFinder.emplace(VK_MBUTTON, "Wheel_Mouse");
	keyFinder.emplace(VK_XBUTTON1, "Mouse X1");
	keyFinder.emplace(VK_XBUTTON2, "Mouse x2");
	keyFinder.emplace(VK_TAB, "Tab");
	keyFinder.emplace(VK_MENU, "Alt");
	keyFinder.emplace(VK_CAPITAL, "CapsLock");
	keyFinder.emplace(VK_PRIOR, "PageUp");
	keyFinder.emplace(VK_NEXT, "PageDown");
	keyFinder.emplace(VK_END, "End");
	keyFinder.emplace(VK_HOME, "Home");
	keyFinder.emplace(VK_LEFT, "LeftArrow");
	keyFinder.emplace(VK_UP, "UpArrow");
	keyFinder.emplace(VK_RIGHT, "RightArrow");
	keyFinder.emplace(VK_DOWN, "DownArrow");
	keyFinder.emplace(VK_SELECT, "SelectKey");
	keyFinder.emplace(VK_INSERT, "Insert");
	keyFinder.emplace(VK_DELETE, "Del");
	keyFinder.emplace(VK_SPACE, "SPACE");
	keyFinder.emplace(VK_F1, "F1");
	keyFinder.emplace(VK_F2, "F2");
	keyFinder.emplace(VK_F3, "F3");
	keyFinder.emplace(VK_F4, "F4");
	keyFinder.emplace(VK_F5, "F5");
	keyFinder.emplace(VK_F6, "F6");
	keyFinder.emplace(VK_F7, "F7");
	keyFinder.emplace(VK_F8, "F8");
	keyFinder.emplace(VK_F9, "F9");
	keyFinder.emplace(VK_F10, "F10");
	keyFinder.emplace(VK_F11, "F11");
	keyFinder.emplace(VK_F12, "F12");
	keyFinder.emplace(VK_LSHIFT, "LShift");
	keyFinder.emplace(VK_RSHIFT, "RShift");
	keyFinder.emplace(VK_LCONTROL, "LControl");
	keyFinder.emplace(VK_RCONTROL, "RControl");
	keyFinder.emplace(VK_LMENU, "LMenu");
	keyFinder.emplace(VK_RMENU, "RMenu");

	// Combo Menu options
	static const char* Type[] = {
		"Closest Hero",
		"Lowest HP",
		"Near Mouse",
		"Less AA"
	};
	
	//SdkUiConsoleWrite("Type: %i", TypeSelection);

	SdkUiBeginTree("Lolhero Half baked",&ObjectExpanded);
	if (ObjectExpanded)
	{
		SdkUiCheckbox("Enable Scripts", &bPred, NULL);				
		SdkUiEndTree();
	}
	
	SdkUiBeginTree("LTargetSelect", &TSExpanded);	
	if (TSExpanded)
	{   
		SdkUiCombo("Mode", &TypeSelection, Type, RTL_NUMBER_OF(Type), &comboChange);
				
		switch (TypeSelection)
		{
			case 0:
			{
				Closest;
			}

			case 1:
			{
				LowestHP;
			}
			case 2:
			{
				NearMouse;
			}	
			case 3:
			{
				LessAA;
			}
			default:
				break;

		}

		if (comboChange) {
			SdkSetSettingNumber("Mode", TypeSelection);
		}
		
		SdkUiCheckbox("Force target LButton",&lClick, &lClicked);

		if (lClicked) {
			SdkSetSettingBool("Force target LButton", lClick);
		}		
		SdkUiEndTree();

	}

	SdkUiBeginTree("Key Settings:", &keySettingsExpand); 
	if (keySettingsExpand) 
	{
		SdkUiText("Combo:");
		SdkUiForceOnSameLine();
		SdkUiSmallButton(ComboLabel, &ComboClicked);
		SdkUiText("UltiR:");	
		SdkUiForceOnSameLine();
		SdkUiSmallButton(UltiLabel, &UltiClicked);
		SdkUiEndTree();

		//SdkUiConsoleWrite("Combo Key %i, Ulti key %i, ComboLabel %s, UltiLabel %s", comboGlobal, ultiGlobal, ComboLabel, UltiLabel);
		
		auto keySearchMap = keyFinder.find(comboGlobal);		

		charComboGlobal = comboGlobal;				
		charComboGlobal = charComboGlobal.append("##1");
		// On click Combo load key label mappings

		if (!ComboClicked && !ComboListenState) {
			if (keySearchMap != keyFinder.end()) {
				std::string id = "##1";
				valueFound = keySearchMap->second + id;
				ComboLabel = valueFound.c_str();
			}
		}

		if (ComboClicked && !ComboListenState) {
				ComboListenState = true;
				ComboLabel = "PRESS";	
		} 				

		// Key Pressed function		
		if (ComboListenState && keyPressed) {	
			ComboListenState = !ComboListenState;
			ComboLabel = charComboGlobal.c_str();
			

			if (keySearchMap != keyFinder.end()) {		
				std::string id = "##1";
				valueFound = keySearchMap->second + id;				
				ComboLabel = valueFound.c_str();
			}				
		} 	

		// Mouse click function Combo -- Begin
		if (ComboListenState && GetKeyState(VK_MBUTTON) < 0)
		{
			ComboListenState = !ComboListenState;
			comboGlobal = VK_MBUTTON;

			if (keySearchMap != keyFinder.end()) {
				std::string id = "##1";
				valueFound = keySearchMap->second + id;
				ComboLabel = valueFound.c_str();
			}
		}	

		if (ComboListenState && GetKeyState(VK_XBUTTON1) < 0)
		{
			ComboListenState = !ComboListenState;
			comboGlobal = VK_XBUTTON1;

			if (keySearchMap != keyFinder.end()) {
				std::string id = "##1";
				valueFound = keySearchMap->second + id;
				ComboLabel = valueFound.c_str();
			}
		}
		
		if (ComboListenState && GetKeyState(VK_XBUTTON2) < 0)
		{
			ComboListenState = !ComboListenState;
			comboGlobal = VK_XBUTTON2;

			if (keySearchMap != keyFinder.end()) {
				std::string id = "##1";
				valueFound = keySearchMap->second + id;
				ComboLabel = valueFound.c_str();
			}
		} // Mouse Click -- End --

		// Ulti key label mappings
		auto ultiSearchMap = keyFinder.find(ultiGlobal);

		charUltiGlobal = ultiGlobal;
		charUltiGlobal = charUltiGlobal.append("##2");

		if (!UltiClicked && !UltiListenState) {
			if (ultiSearchMap != keyFinder.end()) {
				std::string id = "##2";
				ultiFound = ultiSearchMap->second + id;
				UltiLabel = ultiFound.c_str();
			}
		}
		
		if (UltiClicked && !UltiListenState) {
				UltiListenState = true;
				UltiLabel = "PRESS";			
		}

		if (UltiListenState && keyPressed) {						
			UltiListenState = !UltiListenState;
			UltiLabel = charUltiGlobal.c_str();	

			if (ultiSearchMap != keyFinder.end()) {
				std::string id = "##2";
				ultiFound = ultiSearchMap->second + id;				
				UltiLabel = ultiFound.c_str();
			}
		} 	


		if (UltiListenState && GetKeyState(VK_MBUTTON) < 0)
		{
			UltiListenState = !UltiListenState;
			ultiGlobal = VK_MBUTTON;

			if (ultiSearchMap != keyFinder.end()) {
				std::string id = "##2";
				ultiFound = keySearchMap->second + id;
				UltiLabel = ultiFound.c_str();
			}
		}

		if (UltiListenState && GetKeyState(VK_XBUTTON1) < 0)
		{
			UltiListenState = !UltiListenState;
			ultiGlobal = VK_XBUTTON1;

			if (ultiSearchMap != keyFinder.end()) {
				std::string id = "##2";
				ultiFound = keySearchMap->second + id;
				UltiLabel = ultiFound.c_str();
			}
		}

		if (UltiListenState && GetKeyState(VK_XBUTTON2) < 0)
		{
			UltiListenState = !UltiListenState;
			ultiGlobal = VK_XBUTTON2;

			if (ultiSearchMap != keyFinder.end()) {
				std::string id = "##2";
				ultiFound = keySearchMap->second + id;
				UltiLabel = ultiFound.c_str();
			}
		}
					
		saveSettings();	
	}				

	// Keypress callback
	SdkRegisterOnKeyPress
	(
		[](bool KeyDown, unsigned int VirtualKey, unsigned short Repeated, unsigned char ScanCode, bool IsExtended, bool IsAltDown, bool PreviousState, bool TransitionState, void* UserData) -> void
	{
		UNREFERENCED_PARAMETER(UserData);		

		if (ComboListenState) {
			comboGlobal = VirtualKey;				
		}
		
		 if (UltiListenState) {
			ultiGlobal = VirtualKey;				
		  }
		  
		keyPressed = KeyDown;		
				
	}, NULL
	);
}

void __cdecl DrawGameScene( _In_ void* UserData)
{
	UNREFERENCED_PARAMETER(UserData);

	// Update entitymanager
	EntityManager::Update();	

	if (!bPred)
		return;	

	// main script Executes
	auto champName = EntityManager::GetLocalPlayer().GetName();

	if (strcmp(champName, "Kaisa") == 0) {
		Kaisa::Execute();
	}

	if (strcmp(champName, "Xerath") == 0) {
		Xearth::Execute();
	}
	
	/*
	BUFF EXAMPLE
	DRAW BUFF NAMES OF TARGET
	*/
	
	for (auto hero : EntityManager::GetEnemyHeroes())
	{
		if (!hero.isAlive() || !hero.isVisible())
			continue;

		auto net = hero.GetNeutralKills();

		auto buffs = hero.GetBuffs();
		
		for (size_t i = 0; i < buffs.size(); i++)
		{
			auto buff = buffs[i];
			if (!buff.IsValid())
				continue;

			auto Pos = hero.GetPosition();
			Pos.z += (i * 15);

			SdkDrawText(&Pos, NULL, buff.Name, "Arial", &_g_ColorWhite, 18, 5, 0, false);	
		}
	}
	
	for (auto hero : EntityManager::GetAllyHeroes())
	{
		if (!hero.isAlive() || !hero.isVisible())
			continue;

		auto net = hero.GetNeutralKills();

		auto buffs = hero.GetBuffs();

		for (size_t i = 0; i < buffs.size(); i++)
		{
			auto buff = buffs[i];
			if (!buff.IsValid())
				continue;

			auto Pos = hero.GetPosition();
			Pos.z += (i * 15);

			SdkDrawText(&Pos, NULL, buff.Name, "Arial", &_g_ColorWhite, 18, 5, 0, false);

		}
	}
	
	//for (auto object : EntityManager::GetEnemyMinions())
	//{
	//	if (EntityManager::GetLocalPlayer().GetPosition().Distance(object.GetPosition()) > 1000)
	//		continue;
	//	
	//}
}