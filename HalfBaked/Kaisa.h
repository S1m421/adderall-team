#pragma once
#include "SDKEX_Headers.h"
#include <random>
#include "Prediction.h"
#include "TargetSelector.h"

//global TS targets
AttackableUnit targetW;
AttackableUnit targetQ;

namespace Kaisa {
	SDKVECTOR emptyVec = { 0, 0, 0 };		

	void Execute() { // executes in main gameScene

		int Qstate = 0;
		int Wstate = 0;

		SDK_SPELL W;
		SDK_SPELL Q;
		SdkGetAISpell(g_LocalPlayer, 1, &W);	
		SdkGetAISpell(g_LocalPlayer, 0, &Q);
		
		// INIT selected override
		LTargetSelector().setSelectedTarget();	
		
		
		if (GetAsyncKeyState(comboGlobal)) {

			
			// TS setup
			if (lClick && selectedTarget.Object != NULL){
				targetW = selectedTarget;
				targetQ = selectedTarget;
			}
			else {
				targetW = LTargetSelector().returnTarget(TSmode(TypeSelection), W.CastRange);
				targetQ = LTargetSelector().returnTarget(TSmode(TypeSelection), Q.CastRange);
			}
			// End TS Setup

			// W spell
			if (targetW.Object != NULL && targetW.isVisible() && targetW.isEnemy() && targetW.isAlive()) {
				
				auto KaisaW = Linear(&targetW, W.CastDelay, W.MissileSpeed, W.CastRange, W.LineWidth);				
				
				SdkCanAICastSpell(g_LocalPlayer, 0, NULL, &Qstate);
				SdkCanAICastSpell(g_LocalPlayer, 1, NULL, &Wstate);

				if (Wstate == SPELL_CAN_CAST_OK) {
					if (SDKVECTORMATH(KaisaW) != SDKVECTORMATH(0, 0, 0)) {
						if (!Collision(KaisaW, W.LineWidth)) {						  
							SdkCastSpellLocalPlayer(NULL, &KaisaW, 1, 0);
						}
					}
				}				
			}
			// End W spell

			// Q Spell
			if (targetQ.Object != NULL && targetQ.isVisible() && targetQ.isEnemy() && targetQ.isAlive()) {
				if (Qstate == SPELL_CAN_CAST_OK) {
					SdkCastSpellLocalPlayer(g_LocalPlayer, NULL, 0, 0);
				}
			}
			// Ened Q spell
			
		}				
				
	}	
	
};