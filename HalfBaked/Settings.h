#pragma once
#include "SDKEX_Headers.h"

// Used for key settings load and save. Void calls in lpred.cpp

void saveSettings() {
	// Key settings save
	SdkSetSettingNumber("ComboKey", comboGlobal);
	SdkSetSettingNumber("UltiKey", ultiGlobal);
	
}

void loadSettings() {
	// do not touch TS selector
	SdkGetSettingNumber("Mode", &TypeSelection, 0);
	SdkGetSettingBool("Force target LButton", &lClick, false);	

	// Key settings load + defaults
	SdkGetSettingNumber("ComboKey", &comboGlobal, 32);
	SdkGetSettingNumber("UltiKey", &ultiGlobal, 84);
}