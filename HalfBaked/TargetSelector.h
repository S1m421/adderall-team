#pragma once
#include "SDKEX_Headers.h"
#include "Vectormath.h"
#include <random>

#define KEY_DOWN(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0)
#define KEY_UP(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 0 : 1)

enum TSmode
{
   Closest,
   LowestHP,
   NearMouse,
   LessAA
};

AttackableUnit selectedTarget;
AttackableUnit tmpTarget;
bool isClickPressed;

class LTargetSelector {

public:

	LTargetSelector() {
		
	};		

	
	AttackableUnit setSelectedTarget() {			

		if (GetKeyState(VK_LBUTTON) < 0) {
			if (!isClickPressed) {
				selectedTarget = NULL;
				isClickPressed = true;

				for (auto hero : EntityManager::GetEnemyHeroes()) {

					if (!hero.isAlive() || !hero.isVisible() || !hero.isMouseOver())
						continue;

					if (hero.isMouseOver())
						selectedTarget = hero;
				}
			}
		}
		else {
			isClickPressed = false;				
		}	
	return 0;
	}

	SDKVECTOR onCursor() {

		SDKPOINT CurrentScreenCoordinates; SDKVECTOR CurrentWorldCoordinates;
		POINT Cursor; GetCursorPos(&Cursor);
		HWND Window = NULL; SdkUiGetWindow(&Window);
		ScreenToClient(Window, &Cursor);
		CurrentScreenCoordinates.x = (float)Cursor.x;
		CurrentScreenCoordinates.y = (float)Cursor.y;
		SdkScreenToWorld(&CurrentScreenCoordinates, &CurrentWorldCoordinates);
		auto pos = CurrentWorldCoordinates;
		return pos;
	}


	AttackableUnit returnTarget(TSmode type, float close) {		
		
		AttackableUnit lasthero = NULL;		

		if (type == Closest) {
			
			for (auto hero : EntityManager::GetEnemyHeroes()) {

				if (!hero.isAlive() || !hero.isVisible())
					continue;

				float DistanceBetween = SDKVECTORMATH(EntityManager::GetLocalPlayer().GetPosition()).Distance(SDKVECTORMATH(hero.GetPosition()));
				if (DistanceBetween <= close) {
						close = DistanceBetween;
						lasthero = hero;							
				}
			} return lasthero;
		} 
		

		else if (type == LowestHP) {

			float lowestHealth = 10000;

			for (auto hero : EntityManager::GetEnemyHeroes()) {

				if (!hero.isAlive() || !hero.isVisible())
					continue;

				auto Lhealth = hero.GetHealth();
				float DistanceBetween = SDKVECTORMATH(EntityManager::GetLocalPlayer().GetPosition()).Distance(SDKVECTORMATH(hero.GetPosition()));
				if (DistanceBetween <= close) {
					if (Lhealth.Current < lowestHealth) {
						lowestHealth = Lhealth.Current;
						lasthero = hero;
					}
				}
			} return lasthero;	
		}

		else if (type == NearMouse) {
			
			if (GetKeyState(VK_LBUTTON) > 0) {
				
				for (auto hero : EntityManager::GetEnemyHeroes()) {

					if (!hero.isAlive() || !hero.isVisible() || !hero.isMouseOver())
						continue;			

					lasthero = hero;
				}
			} return lasthero;
		}	

		else if (type == LessAA) {

			auto ad = EntityManager::GetLocalPlayer().GetAttackDamage();			
			auto lowestAA = 5000;

			for (auto hero : EntityManager::GetEnemyHeroes()) {
				if (!hero.isAlive() || !hero.isVisible())
					continue;

				auto armor = hero.GetArmor();
				auto healthCurr = hero.GetHealth().Current;
				auto aaCount = (healthCurr + armor) / ad;				

				if (aaCount < lowestAA) {
					lowestAA = aaCount;
					lasthero = hero;
				}	
			} return lasthero;
		}
	}	
};