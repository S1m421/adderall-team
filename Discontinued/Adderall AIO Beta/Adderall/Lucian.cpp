#include "Include.h"

long Lucian_lastAttackTime;

void Lucian::Create()
{
	Menu::Create();
	Events::Create();
}

long Lucian_getTime()
{
	SYSTEMTIME time;
	GetSystemTime(&time);
	return time.wSecond * 1000 + time.wMilliseconds;
}

void Lucian_onAIAttack(void* ai, void* target_object, bool start_attack, bool stop_attack, void* user_data)
{
	if (ai == pLocalPlayer->Hero->BaseObject)
	{
		if (!stop_attack) { Lucian_lastAttackTime = Lucian_getTime(); }
		else { Lucian_lastAttackTime = 0; }
	}
}

void Lucian::Combo()
{
	auto WData = pLocalPlayer->Hero->GetSpell(1);

	auto Qstate = pLocalPlayer->Hero->CanCastSpell(0);
	auto Wstate = pLocalPlayer->Hero->CanCastSpell(1);
	auto Estate = pLocalPlayer->Hero->CanCastSpell(2);

	auto LucianTarget_Q = pTargetSelector->GetTarget(pLocalPlayer->Hero->GetSpell(0).CastRange, DamageType::Physical);
	auto LucianTarget_W = pTargetSelector->GetTarget(pLocalPlayer->Hero->GetSpell(1).CastRange, DamageType::Physical);
	auto LucianTarget_E = pTargetSelector->GetTarget(901, DamageType::Physical);
	
	float attackCastDelay;
	SdkGetAIAttackCastDelay(pLocalPlayer->Hero->BaseObject, &attackCastDelay);
	bool isAfterAA = Lucian_lastAttackTime != 0 && Lucian_getTime() - Lucian_lastAttackTime > attackCastDelay * 1000.0f;
	if (isAfterAA) { Lucian_lastAttackTime = 0; }

	if (LucianTarget_E != nullptr)
	{
		if (pMenu->GetBoolean("Lucian.Combo.E"))
		{
			if ((LucianTarget_Q) && (Estate == false) && (Qstate == true) &&
				(pLocalPlayer->Hero->HasBuff("LucianPassiveBuff") == false))
			{
				pLocalPlayer->CastSpell(LucianTarget_Q, 0);
			}

			if ((LucianTarget_W) && (Estate == false) && (Wstate == true) &&
				(pLocalPlayer->Hero->HasBuff("LucianPassiveBuff") == false))
			{
				pLocalPlayer->CastSpell(LucianTarget_W->GetPosition(), 1);
			}

			if ((LucianTarget_E) && (Estate == true) &&
				(pLocalPlayer->Hero->HasBuff("LucianPassiveBuff") == false))
			{
				SDKPOINT CurrentScreenCoordinates; SDKVECTOR CurrentWorldCoordinates;
				POINT Cursor; GetCursorPos(&Cursor);
				HWND Window = NULL; SdkUiGetWindow(&Window);
				ScreenToClient(Window, &Cursor);
				CurrentScreenCoordinates.x = (float)Cursor.x;
				CurrentScreenCoordinates.y = (float)Cursor.y;
				SdkScreenToWorld(&CurrentScreenCoordinates, &CurrentWorldCoordinates);

				pLocalPlayer->CastSpell(CurrentWorldCoordinates, 2);
			}
		}
		else
		{
			if ((LucianTarget_Q) && (Qstate == true) &&
				(pLocalPlayer->Hero->HasBuff("LucianPassiveBuff") == false))
			{
				pLocalPlayer->CastSpell(LucianTarget_Q, 0);
			}

			if ((LucianTarget_W) && (Wstate == true) &&
				(pLocalPlayer->Hero->HasBuff("LucianPassiveBuff") == false))
			{
				pLocalPlayer->CastSpell(LucianTarget_W->GetPosition(), 1);
			}
		}
	}
}
