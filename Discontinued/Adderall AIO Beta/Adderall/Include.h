#pragma once

#define _USE_MATH_DEFINES

/* ---- Core & SDK ---- */
#include "Menu.h"
#include <Windows.h>
#include <map>
#include <vector>
#include <functional>
#include <ctime>
#include <algorithm>
#include <string>
#include <math.h>
#include "sdkapi.h"
#include "d3dx9.h"
#include "Vectormath.h"

/* ----- Wrapper ----- */
#include "NiicePlugin.h"
#include "NiiceTargetSelector.h"
#include "NiiceOrbwalker.h"
#include "Prediction.h"

/* ----- Common ----- */
#include "Events.h"


/* ----- Champions ----- */
#include "Kaisa.h"
#include "Lucian.h"
#include "Cassiopeia.h"

extern PSDK_CONTEXT SDK_CONTEXT_GLOBAL;
extern std::shared_ptr<ISDK> pSDK;
extern std::shared_ptr<IMenu> pMenu;
extern std::shared_ptr<ILocalPlayer> pLocalPlayer;
extern std::shared_ptr<ITargetSelector> pTargetSelector;
extern std::shared_ptr<IOrbwalker> pOrbwalker;