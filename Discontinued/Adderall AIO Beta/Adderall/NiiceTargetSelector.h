#pragma once

#include <Windows.h>
#include <map>
#include <vector>
#include <functional>
#include <string>

#include "sdkapi.h"

extern PSDK_CONTEXT SDK_CONTEXT_GLOBAL;

#define LIBRARY_NAME_TARGETSELECTOR "NiiceTargetSelector"
#define LIBRARY_VERSION_TARGETSELECTOR 2
#define LIBRARY_IMPORT_TARGETSELECTOR "NiiceTargetSelector"

enum TargetingMode
{
	AutoPriority,
	LowHP,
	MostAD,
	MostAP,
	Closest,
	NearMouse
};

enum DamageType
{
	Physical,
	Magical,
	True
};

struct ITargetSelector
{
	virtual ~ITargetSelector() = default;

	virtual float GetPriority(std::shared_ptr<IHero> hero) = 0;
	virtual bool IsInvulnerable(std::shared_ptr<IHero> hero, DamageType type) = 0;
	virtual bool IsValidTarget(std::shared_ptr<IHero> hero, float range, DamageType type) = 0;
	virtual std::shared_ptr<IHero> GetTarget(float range, DamageType type) = 0;
	virtual float GetRealAutoAttackRange(std::shared_ptr<IAI> target) = 0;

	TargetingMode ActiveMode;
	std::shared_ptr<IHero> SelectedTarget;
};
