#pragma once

#include <Windows.h>
#include <map>
#include <vector>
#include <functional>
#include <string>

#include "sdkapi.h"

extern PSDK_CONTEXT SDK_CONTEXT_GLOBAL;

#define LIBRARY_NAME_ORBWALKER "NiiceOrbwalker"
#define LIBRARY_VERSION_ORBWALKER 5
#define LIBRARY_IMPORT_ORBWALKER "NiiceOrbwalker"

enum OrbwalkingMode
{
	Lasthit,
	Mixed,
	Clear,
	Combo,
	Flee,
	None
};

struct IOrbwalker
{
	virtual ~IOrbwalker() = default;

	virtual void AddBeforeAttackEvent(std::function<void(std::shared_ptr<IAI>)> callback) = 0;
	virtual void AddAfterAttackEvent(std::function<void(std::shared_ptr<IAI>)> callback) = 0;
	virtual void EnableMovement() = 0;
	virtual void DisableMovement() = 0;
	virtual void EnableAttack() = 0;
	virtual void DisableAttack() = 0;

	OrbwalkingMode ActiveMode;
	std::shared_ptr<IAI> Target;
};
