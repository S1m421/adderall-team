#pragma once

namespace Modes
{
	void Always();
	void Combo();
	void Mixed();
	void Lasthit();
	void Clear();
	void Flee();
};