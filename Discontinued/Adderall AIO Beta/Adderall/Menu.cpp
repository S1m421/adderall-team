#include "Include.h"

enum ChampionList
{
	AHRI, ASHE, BRAND, CASSIOPEIA,
	EZREAL, KAISA, // LolHero Logic [WIP]
	KOGMAW, SIVIR, LUCIAN,
	// XERATH, // Disabled [Adderall]
	UNKNOWN
}; ChampionList UserHero = ChampionList::UNKNOWN;

bool AdderallInBlood;

void Menu::Create() {

	if (pLocalPlayer->Hero->GetName() == "Ashe")
		UserHero = ChampionList::ASHE;
	else if (pLocalPlayer->Hero->GetName() == "KogMaw")
		UserHero = ChampionList::KOGMAW;
	else if (pLocalPlayer->Hero->GetName() == "Kaisa")
		UserHero = ChampionList::KAISA;
	else if (pLocalPlayer->Hero->GetName() == "Ezreal")
		UserHero = ChampionList::EZREAL;
	else if (pLocalPlayer->Hero->GetName() == "Ahri")
		UserHero = ChampionList::AHRI;
	else if (pLocalPlayer->Hero->GetName() == "Cassiopeia")
		UserHero = ChampionList::CASSIOPEIA;
	else if (pLocalPlayer->Hero->GetName() == "Sivir")
		UserHero = ChampionList::SIVIR;
	else if (pLocalPlayer->Hero->GetName() == "Brand")
		UserHero = ChampionList::BRAND;
	else if (pLocalPlayer->Hero->GetName() == "Lucian")
		UserHero = ChampionList::LUCIAN;
	else
		UserHero = ChampionList::UNKNOWN;

MenuId = pSDK->AddMenu([](void* userData)
{
	pMenu->AddWindow("1337.Window", "Adderall Beta", true, false, []()
	{
		switch (UserHero)
		{
			case ChampionList::UNKNOWN:
				AdderallInBlood = false;
				SdkUiText("Your Champion is not Supported");
				break;

			case ChampionList::KAISA:
				AdderallInBlood = true;
				pMenu->AddBulletText("1337 Kaisa Skriptos - Ez Chally");
				pMenu->AddTree("1337.Combo", "Combo:", false, []()
				{
					pMenu->AddCheckbox("1337.Combo.Q", "Q Combo", true);
					pMenu->AddCheckbox("1337.Combo.W", "W Combo", true);
					pMenu->AddCheckbox("1337.Combo.WKS", "W only KS", true);					
				});
				break;
			case ChampionList::LUCIAN:
				AdderallInBlood = true;
				pMenu->AddTree("Lucian.Combo", "Combo Logic", false, []()
				{
					pMenu->AddCheckbox("Lucian.Combo.E", "Dash in Combo", true);
					if (!pMenu->GetBoolean("Lucian.Combo.E"))
						pMenu->AddBulletText("Q->AA->W->AA");
					if (pMenu->GetBoolean("Lucian.Combo.E"))
					pMenu->AddBulletText("E->AA->Q->AA->W->AA");
				});
				break;
			case ChampionList::CASSIOPEIA:
				AdderallInBlood = true;
				pMenu->AddTree("Cassiopeia.Combo", "Combo Logic", false, []()
				{
					pMenu->AddCheckbox("Cassiopeia.Combo.Q", "Auto Q", true);
					pMenu->AddCheckbox("Cassiopeia.Combo.W", "Auto W", true);
					pMenu->AddCheckbox("Cassiopeia.Combo.E", "Auto E", true);
					/* pMenu->AddCheckbox("Cassiopeia.Combo.R", "Auto R", true); */
					if (pLocalPlayer->Hero->HasItem(3116))
						pMenu->AddBulletText("E->Q->E->E (Circunstancial W)");
					else
						pMenu->AddBulletText("Q->E->E (Circunstancial W)");
				});
				pMenu->AddTree("Cassiopeia.Harass", "Harass Logic", false, []()
				{
					pMenu->AddCheckbox("Cassiopeia.Harass.Q", "Harass Q", true);
					if (pMenu->GetBoolean("Cassiopeia.Harass.Q"))
					pMenu->AddCheckbox("Cassiopeia.Harass.QS", "Safer Q", true);
					pMenu->AddCheckbox("Cassiopeia.Harass.E", "Harass E", true);
					if (pMenu->GetBoolean("Cassiopeia.Harass.QS"))
						pMenu->AddBulletText("Slower & Precise Harass");
					else
						pMenu->AddBulletText("Faster Harass");
				});
				pMenu->AddTree("Cassiopeia.LastHit", "LastHit Logic", false, []()
				{
					pMenu->AddCheckbox("Cassiopeia.LastHit.E", "LastHit E", true);
					if (pMenu->GetBoolean("Cassiopeia.LastHit.E"))
						pMenu->AddCheckbox("Cassiopeia.LastHit.AA", "Block AA", true);
					if (pMenu->GetBoolean("Cassiopeia.LastHit.AA"))
						pMenu->AddBulletText("Just E for LastHit");
					else
						pMenu->AddBulletText("AA & E for Lasthit");
				});
				pMenu->AddTree("Cassiopeia.Clear", "Clear Logic", false, []()
				{
					pMenu->AddCheckbox("Cassiopeia.Clear.Wave", "Clear Wave", true);
					pMenu->AddCheckbox("Cassiopeia.Clear.Jungle", "Clear Jungle", true);
					pMenu->AddBulletText("Mana Manager Someday");
				});
				break;
		}
	});
}); }

void Menu::Remove()
{
	pSDK->RemoveOverlayEvent(MenuId);
}