#include "Include.h"

void Events::Create()
{
	GameUpdateId = pSDK->AddGameEvent(GameUpdate);
}

void Events::Remove()
{
	pSDK->RemoveGameEvent(GameUpdateId);
}

void Events::GameUpdate(void* userData)
{
	if (pSDK->IsChatOpen() || pSDK->IsConsoleOpen() || !pSDK->IsWindowInForeground())
		return;	

		if (pOrbwalker->ActiveMode == OrbwalkingMode::Combo)
		{
			if (pLocalPlayer->Hero->GetName() == "Kaisa")
				Kaisa::Combo();

			if (pLocalPlayer->Hero->GetName() == "Lucian")
				Lucian::Combo();

			if (pLocalPlayer->Hero->GetName() == "Cassiopeia")
				Cassiopeia::Combo();
		}

		if (pOrbwalker->ActiveMode == OrbwalkingMode::Mixed)
		{
			if (pLocalPlayer->Hero->GetName() == "Cassiopeia")
				Cassiopeia::Mixed();
		}

	if (pOrbwalker->ActiveMode == OrbwalkingMode::Lasthit)
	{
		if (pLocalPlayer->Hero->GetName() == "Cassiopeia")
			Cassiopeia::Lasthit();
	}
	
	if (pOrbwalker->ActiveMode == OrbwalkingMode::Clear)
	{
		if (pLocalPlayer->Hero->GetName() == "Cassiopeia")
			Cassiopeia::Clear();
	}
	
	if (pOrbwalker->ActiveMode == OrbwalkingMode::Flee)
	{
		// Nothing Here Yet
	}
	
	if (pLocalPlayer->Hero->GetName() == "Kaisa")
		Kaisa::Always();
}