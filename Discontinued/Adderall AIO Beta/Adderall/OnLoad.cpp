#include "Include.h"

PSDK_CONTEXT SDK_CONTEXT_GLOBAL;

std::shared_ptr<ISDK> pSDK;
std::shared_ptr<IMenu> pMenu;
std::shared_ptr<ILocalPlayer> pLocalPlayer;
std::shared_ptr<ITargetSelector> pTargetSelector;
std::shared_ptr<IOrbwalker> pOrbwalker;

BOOL WINAPI DllMain(_In_ HINSTANCE hinstDLL, _In_ DWORD fdwReason, _In_ LPVOID lpvReserved)
{
	if (fdwReason == DLL_PROCESS_ATTACH)
	{
		SDK_EXTRACT_CONTEXT(lpvReserved);

		if (!SDK_CONTEXT_GLOBAL || !SDKSTATUS_SUCCESS(SdkNotifyLoadedModule("Adderall Beta", SDK_VERSION)))
			return FALSE;

		SdkRegisterOnLoad([](void* userData) {
			auto SizeSDK = sizeof pSDK;
			auto SizeMenu = sizeof pMenu;
			auto SizeLocalPlayer = sizeof pLocalPlayer;
			auto SizeTargetSelector = sizeof pTargetSelector;
			auto SizeOrbwalker = sizeof pOrbwalker;
			const auto statusSDK = SdkGetLibraryImport(LIBRARY_NAME_PLUGIN, LIBRARY_IMPORT_SDK, LIBRARY_VERSION_PLUGIN, &pSDK, &SizeSDK);
			const auto statusMenu = SdkGetLibraryImport(LIBRARY_NAME_PLUGIN, LIBRARY_IMPORT_MENU, LIBRARY_VERSION_PLUGIN, &pMenu, &SizeMenu);
			const auto statusLocalPlayer = SdkGetLibraryImport(LIBRARY_NAME_PLUGIN, LIBRARY_IMPORT_LOCALPLAYER, LIBRARY_VERSION_PLUGIN, &pLocalPlayer, &SizeLocalPlayer);
			const auto statusTargetSelector = SdkGetLibraryImport(LIBRARY_NAME_TARGETSELECTOR, LIBRARY_IMPORT_TARGETSELECTOR, LIBRARY_VERSION_TARGETSELECTOR, &pTargetSelector, &SizeTargetSelector);
			const auto statusOrbwalker = SdkGetLibraryImport(LIBRARY_NAME_ORBWALKER, LIBRARY_IMPORT_ORBWALKER, LIBRARY_VERSION_ORBWALKER, &pOrbwalker, &SizeOrbwalker);

			if (!SDKSTATUS_SUCCESS(statusSDK) || !SDKSTATUS_SUCCESS(statusMenu) || !SDKSTATUS_SUCCESS(statusLocalPlayer) || !SDKSTATUS_SUCCESS(statusTargetSelector) || !SDKSTATUS_SUCCESS(statusOrbwalker))
			{
				SdkUiConsoleWrite("[Adderall] Dependencies missing.");
				return;
			}

			/* [Adderall] Champion Check -> Begin*/ // I dont know how to extend it for a champion list.

			if (pLocalPlayer->Hero->GetName() == "Kaisa")
			{
				Kaisa::Create();
				pSDK->PrintChat("Half Baked Library by LolHero has been Loaded", pLocalPlayer->Hero->GetName().c_str());
				pSDK->PrintChat("Challenger <b>%s</b> v1.0", pLocalPlayer->Hero->GetName().c_str());
				return;			
			}

			if (pLocalPlayer->Hero->GetName() == "Lucian")
			{
				Lucian::Create();
				pSDK->PrintChat("Half Baked Library by LolHero has been Loaded", pLocalPlayer->Hero->GetName().c_str());
				pSDK->PrintChat("Adderall <b>%s</b> Beta has been Loaded", pLocalPlayer->Hero->GetName().c_str());
				return;
			}

			if (pLocalPlayer->Hero->GetName() == "Cassiopeia")
			{
				Cassiopeia::Create();
				pSDK->PrintChat("Half Baked Library by LolHero has been Loaded", pLocalPlayer->Hero->GetName().c_str());
				pSDK->PrintChat("Adderall <b>%s</b> Beta has been Loaded", pLocalPlayer->Hero->GetName().c_str());
				return;
			}
				
			if (pLocalPlayer->Hero->GetName() != "Kaisa" || pLocalPlayer->Hero->GetName() != "Lucian" || pLocalPlayer->Hero->GetName() != "Cassoiopeia")
			{
				SdkUiConsoleWrite("You're not playing a supported champ!");				
				return;
			}
			
		}, nullptr);
	}
	else if (fdwReason == DLL_PROCESS_DETACH)
	{
		Events::Remove();
		Menu::Remove();		
	}

	return TRUE;
}