#pragma once

#include <Windows.h>
#include <map>
#include <vector>
#include <functional>
#include <string>

#include "sdkapi.h"

extern PSDK_CONTEXT SDK_CONTEXT_GLOBAL;

#define LIBRARY_NAME_PLUGIN "NiicePlugin"
#define LIBRARY_VERSION_PLUGIN 4
#define LIBRARY_IMPORT_SDK "NiiceSDK"
#define LIBRARY_IMPORT_MENU "NiiceMenu"
#define LIBRARY_IMPORT_LOCALPLAYER "NiiceLocalPlayer"

inline bool operator!=(const SDKVECTOR& lhs, const SDKVECTOR& rhs)
{
	return
		lhs.x != rhs.x ||
		lhs.y != rhs.y ||
		lhs.z != rhs.z;
}

inline bool operator==(const SDKVECTOR& lhs, const SDKVECTOR& rhs)
{
	return
		lhs.x == rhs.x &&
		lhs.y == rhs.y &&
		lhs.z == rhs.z;
}

inline SDKVECTOR operator-(const SDKVECTOR& lhs, const SDKVECTOR& rhs)
{
	return {
		lhs.x - rhs.x,
		lhs.y - rhs.y,
		lhs.z - rhs.z,
	};
}

inline SDKVECTOR operator+(const SDKVECTOR& lhs, const SDKVECTOR& rhs)
{
	return {
		lhs.x + rhs.x,
		lhs.y + rhs.y,
		lhs.z + rhs.z,
	};
}

inline SDKVECTOR operator*(const SDKVECTOR& lhs, const float& rhs)
{
	return {
		lhs.x * rhs,
		lhs.y * rhs,
		lhs.z * rhs,
	};
}

inline float Magnitude(const SDKVECTOR vec)
{
	return sqrt(vec.x * vec.x + vec.y * vec.y + vec.z * vec.z);
}

inline SDKVECTOR Normalize(const SDKVECTOR vec)
{
	const auto magnitude = Magnitude(vec);

	return {
		vec.x / magnitude,
		vec.y / magnitude,
		vec.z / magnitude
	};
}

inline SDKVECTOR Perpendicular(const SDKVECTOR vec, const int offset = 0)
{
	return offset == 0 ? SDKVECTOR{ -vec.y, vec.x, vec.z } : SDKVECTOR{ vec.y, -vec.x, vec.z };
}

inline SDKVECTOR Extend(const SDKVECTOR vec, const SDKVECTOR toVec, const float distance)
{
	return vec + Normalize(toVec - vec) * distance;
}


inline SDKPOINT operator-(const SDKPOINT& lhs, const SDKPOINT& rhs)
{
	return {
		lhs.x - rhs.x,
		lhs.y - rhs.y
	};
}

inline SDKPOINT operator+(const SDKPOINT& lhs, const SDKPOINT& rhs)
{
	return {
		lhs.x + rhs.x,
		lhs.y + rhs.y
	};
}

inline std::string Format(const std::string format, ...)
{
	va_list args;
	va_start(args, format);
	auto size = _vscprintf(format.c_str(), args);
	std::string result(++size, 0);
	vsnprintf_s(const_cast<char*>(result.data()), size, _TRUNCATE, format.c_str(), args);
	va_end(args);

	return result;
}

inline bool Contains(std::string haystack, std::string needle)
{
	std::transform(haystack.begin(), haystack.end(), haystack.begin(), tolower);
	std::transform(needle.begin(), needle.end(), needle.begin(), tolower);

	return haystack.find(needle) != std::string::npos;
}

inline bool Contains(std::vector<std::string> haystack, std::string needle)
{
	return std::find_if(haystack.begin(), haystack.end(), [&](std::string& entry)
	{
		if (entry.size() != needle.size())
			return false;

		return std::equal(entry.cbegin(), entry.cend(), needle.cbegin(), needle.cend(), [](auto c1, auto c2) { return ::tolower(c1) == ::tolower(c2); });
	}) != haystack.end();
}

struct ISkin
{
	int Id;
	std::string Name;
};

struct IGame
{
	__int64 Id;
	std::string Mode;
	unsigned MapId;
};

struct IObject
{
	virtual ~IObject() = default;
	void* BaseObject;
	virtual int GetTypeFlags() = 0;
	virtual int GetTypeId() = 0;
	virtual std::string GetTypeName() = 0;
	virtual unsigned GetId() = 0;
	virtual unsigned GetNetworkId() = 0;
	virtual std::string GetObjectName() = 0;
	virtual SDKBOX GetBoundingBox() = 0;
	virtual float GetBoundingRadius() = 0;

	virtual bool IsHero() = 0;
	virtual bool IsMinion() = 0;
	virtual bool IsWard() = 0;
	virtual bool IsTurret() = 0;
	virtual bool IsAI() = 0;
	virtual bool IsZombie() = 0;
	virtual bool IsUnit() = 0;
	virtual bool IsSpellMissile() = 0;
	virtual bool IsVisibleOnScreen() = 0;

	virtual int GetTeam() = 0;

	virtual unsigned GetRespawns() = 0;

	virtual float GetDistance(SDKVECTOR position) = 0;
	virtual float GetDistance(std::shared_ptr<IObject> object) = 0;

	virtual SDKVECTOR GetPosition() = 0;
	virtual SDKVECTOR GetOrientation() = 0;
	virtual SDKVECTOR GetVelocity() = 0;
	virtual SDKVECTOR GetAcceleration() = 0;

};

struct IAttackableUnit : IObject
{
	virtual ~IAttackableUnit()
	{
	}

	virtual bool IsDead() = 0;
	virtual bool IsVisible() = 0;
	virtual bool IsInvulnerable() = 0;
	virtual int GetTargetability() = 0;

	virtual bool IsMouseOver() = 0;

	virtual float GetDeathTime() = 0;

	virtual SDK_HEALTH GetHealth() = 0;
};

struct IEnemy
{
	unsigned Id;
	std::shared_ptr<IAttackableUnit> Target;
};

struct IActiveSpell
{
	bool Valid;
	SDK_SPELL_CAST SpellCast;
	void* ParticleObject;
	float EndTime;
	bool SpellWasCast;
	bool IsCharging;
	bool IsChanneling;
	float ChannelEndTime;
	bool IsStopped;
};

struct INavData
{
	SDKVECTOR StartPosition;
	SDKVECTOR EndPosition;
	size_t NextWaypoint;
	size_t NumberOfWaypoints;
	SDKVECTOR* Waypoints;
	SDKVECTOR Velocity;
	bool IsDash;
	float DashSpeed;
	float DashGravity;
};

struct IBuff
{
	unsigned char Type;
	float StartTime;
	float EndTime;
	std::string Name;
	std::shared_ptr<IObject> Caster;
	int Stacks;
	bool HasCounting;
	int Count;
	SDK_SPELL* Spell;
};

struct IAI : IAttackableUnit
{
	virtual ~IAI()
	{
	}

	virtual int GetSkin() = 0;
	virtual void SetSkin(int id) = 0;

	virtual SDKVECTOR GetHealthbarWorld() = 0;
	virtual SDKPOINT GetHealthbarScreen() = 0;

	virtual SDK_SPELL GetBasicAttack() = 0;

	virtual std::string GetName() = 0;
	virtual bool IsPlayer() = 0;
	virtual bool IsBot() = 0;
	virtual bool IsMelee() = 0;
	virtual bool IsRanged() = 0;

	virtual bool CanAttack() = 0;
	virtual bool CanCrit() = 0;
	virtual bool CanCast() = 0;
	virtual bool CanCastSpell(unsigned char slot) = 0;
	virtual bool CanQueueSpell(unsigned char slot) = 0;
	virtual bool CanMove() = 0;

	virtual int CountHeroesInRange(float range, bool enemies = true, bool allies = true, bool target = false, bool myself = false) = 0;

	virtual float GetAutoAttackDamage(std::shared_ptr<IAI> object, bool usePassive = false) = 0;
	virtual float GetPhysicalDamage(std::shared_ptr<IAI> object, float amount) = 0;
	virtual float GetMagicDamage(std::shared_ptr<IAI> object, float amount) = 0;
	virtual bool InAutoAttackRange(std::shared_ptr<IAttackableUnit> object) = 0;

	virtual bool IsAttacking() = 0;
	virtual bool IsMoving() = 0;
	virtual bool IsCasting() = 0;

	virtual IEnemy GetTarget() = 0;

	virtual bool IsBlinded() = 0;
	virtual bool IsStealthed() = 0;
	virtual bool IsTaunted() = 0;
	virtual bool IsFeared() = 0;
	virtual bool IsFleeing() = 0;
	virtual bool IsSuppressed() = 0;
	virtual bool IsSleeping() = 0;
	virtual bool IsNearSighted() = 0;
	virtual bool IsGhosted() = 0;
	virtual bool IsStunned() = 0;
	virtual bool IsCharmed() = 0;
	virtual bool IsSlowed() = 0;
	virtual bool IsSelectable() = 0;
	virtual bool IsCritImmune() = 0;
	virtual bool IsGrounded() = 0;
	virtual bool IsObscured() = 0;
	virtual bool IsKillable() = 0;

	virtual bool HasBuff(std::string name) = 0;
	virtual int GetBuffCount(std::string name) = 0;

	virtual bool IsFacing(std::shared_ptr<IAI> target) = 0;
	virtual bool IsFacing(SDKVECTOR targetPosition) = 0;
	virtual SDKVECTOR GetFacingDirection() = 0;

	virtual float GetArmor() = 0;
	virtual float GetBonusArmor() = 0;
	virtual float GetLethality() = 0;
	virtual float GetBonusArmorPenetration() = 0;
	virtual float GetArmorPenetrationFlat() = 0;
	virtual float GetArmorPenetrationPercent() = 0;
	virtual float GetAttackSpeed() = 0;
	virtual float GetAbilityPower() = 0;
	virtual float GetAttackDamage() = 0;
	virtual float GetBaseAttackDamage() = 0;
	virtual float GetBonusAttackDamage() = 0;
	virtual float GetAttackRange() = 0;
	virtual float GetMagicResist() = 0;
	virtual float GetBonusMagicResist() = 0;
	virtual float GetTenacity() = 0;
	virtual float GetMovementSpeed() = 0;
	virtual float GetCritChance() = 0;
	virtual float GetCritDamageMultiplier() = 0;
	virtual float GetDodgeChance() = 0;
	virtual float GetHealthRegen() = 0;
	virtual float GetMagicPenetrationFlat() = 0;
	virtual float GetMagicPenetrationPercent() = 0;
	virtual float GetLifeSteal() = 0;
	virtual float GetSpellVamp() = 0;
	virtual float GetCooldownReduction() = 0;

	virtual float GetCurrentGold() = 0;
	virtual float GetTotalGold() = 0;
	virtual float GetDeathDuration() = 0;
	virtual SDK_SPELL GetSpell(unsigned char slot) = 0;
	virtual IActiveSpell GetActiveSpell() = 0;

	virtual INavData GetNavData() = 0;

	virtual SDK_ABILITY_RESOURCE GetAbilityResource(unsigned char slot) = 0;
	virtual float GetAbilityResourceRegen(unsigned char slot) = 0;

	virtual float GetAttackDelay() = 0;
	virtual float GetAttackCastDelay() = 0;

	virtual IBuff GetBuff(std::string name) = 0;
	virtual std::vector<IBuff> GetBuffs() = 0;
};

struct MissileCaster
{
	std::shared_ptr<IAI> Caster;
	unsigned CasterId;
};

struct MissileTarget
{
	SDKVECTOR TargetPosition;
	std::shared_ptr<IAttackableUnit> Target;
	unsigned TargetId;
};

struct HeroPerk
{
	unsigned int Id;
	std::string Name;
};

struct IMissile : IObject
{
	virtual ~IMissile()
	{
	}

	virtual SDK_SPELL GetSpell() = 0;
	virtual MissileCaster GetCaster() = 0;
	virtual SDKVECTOR GetStartPosition() = 0;
	virtual bool IsAutoAttack() = 0;
	virtual float GetSpeed() = 0;
	virtual MissileTarget GetTarget() = 0;
	virtual bool HasCompleted() = 0;
	virtual float GetStartTime() = 0;
	virtual float GetWidth() = 0;
};

struct IMinion : IAI
{
	virtual ~IMinion()
	{
	}

	virtual int GetType() = 0;
	virtual int GetLevel() = 0;

	virtual float GetSpawnTime() = 0;
	virtual int GetCampNumber() = 0;

	virtual float GetVisionRadius() = 0;

	virtual bool IsLaneMinion() = 0;
};

struct ITurret : IAI
{
	virtual ~ITurret()
	{
	}

	virtual int GetTurretPosition() = 0;
	virtual int GetTurretLane() = 0;
};

struct IHero : IAI
{
	virtual ~IHero()
	{
	}

	virtual int GetLevel() = 0;
	virtual float GetExperience() = 0;
	virtual int GetNeutralKills() = 0;
	virtual SDKVECTOR GetSpawn() = 0;

	virtual std::vector<HeroPerk> GetPerks() = 0;
	virtual bool HasPerk(unsigned int id) = 0;

	virtual bool HasItem(int id) = 0;
	virtual SDK_ITEM GetItem(unsigned char slot) = 0;
};

struct ILocalPlayer
{
	explicit ILocalPlayer(std::shared_ptr<IHero> hero)
		: Hero(hero)
	{
	}

	virtual ~ILocalPlayer() = default;
	std::shared_ptr<IHero> Hero;

	virtual void AttackTarget(std::shared_ptr<IAttackableUnit> target, bool usePet = false) = 0;
	virtual void AttackMove(SDKVECTOR position) = 0;

	virtual void MoveTo(SDKVECTOR position, bool usePet = false) = 0;

	virtual void Stop(bool usePet = false) = 0;

	virtual void LevelSpell(unsigned char slot) = 0;
	virtual void CastSpell(std::shared_ptr<IAttackableUnit> target, unsigned char slot) = 0;
	virtual void CastSpell(SDKVECTOR position, unsigned char slot) = 0;
	virtual void CastSpell(unsigned char slot) = 0;
	virtual void StartChargeSpell(std::shared_ptr<IAttackableUnit> target, unsigned char slot) = 0;
	virtual void StartChargeSpell(SDKVECTOR position, unsigned char slot) = 0;
	virtual void StartChargeSpell(unsigned char slot) = 0;
	virtual void FinishChargeSpell(std::shared_ptr<IAttackableUnit> target, unsigned char slot) = 0;
	virtual void FinishChargeSpell(SDKVECTOR position, unsigned char slot) = 0;
	virtual void FinishChargeSpell(unsigned char slot) = 0;
	virtual void CastSpell(std::shared_ptr<IAttackableUnit> target, SDKVECTOR position, unsigned char slot, unsigned char state) = 0;
	virtual void UseItem(unsigned char slot) = 0;
	virtual void UseItem(std::shared_ptr<IAttackableUnit> target, unsigned char slot) = 0;
	virtual void UseItem(SDKVECTOR position, unsigned char slot) = 0;
	virtual void UseItem(std::shared_ptr<IAttackableUnit> target, SDKVECTOR position, unsigned char slot) = 0;

	virtual bool BuyItem(int id) = 0;
	virtual void SellItem(int slot) = 0;

};

struct ISDK
{
	virtual ~ISDK() = default;

	virtual int AddMenu(RENDERSCENECALLBACK callback) = 0;

	virtual int AddOverlayEvent(RENDERSCENECALLBACK callback) = 0;
	virtual void RemoveOverlayEvent(int id) = 0;
	virtual int AddGameEvent(RENDERSCENECALLBACK callback) = 0;
	virtual int AddGameEvent(std::function<void()> callback) = 0;
	virtual void RemoveGameEvent(int id) = 0;
	virtual int AddMoveCallback(ONAIMOVECALLBACK callback) = 0;
	virtual void RemoveMoveCallback(int id) = 0;
	virtual int AddCastAttackCallback(ONAICASTATTACKCALLBACK callback) = 0;
	virtual void RemoveCastAttackCallback(int id) = 0;
	virtual int AddAttackCallback(ONAIATTACKCALLBACK callback) = 0;
	virtual void RemoveAttackCallback(int id) = 0;
	virtual int AddObjectCreateCallback(GAMEOBJECTSCALLBACK callback) = 0;
	virtual void RemoveObjectCreateCallback(int id) = 0;
	virtual int AddObjectDeleteCallback(GAMEOBJECTSCALLBACK callback) = 0;
	virtual void RemoveObjectDeleteCallback(int id) = 0;
	virtual int AddObjectUpdateCallback(GAMEOBJECTSCALLBACK callback) = 0;
	virtual void RemoveObjectUpdateCallback(int id) = 0;
	virtual int AddRecallCallback(ONUNITRECALLCALLBACK callback) = 0;
	virtual void RemoveRecallCallback(int id) = 0;

	virtual std::shared_ptr<IObject> AsObject(void* object) = 0;
	virtual std::shared_ptr<IMissile> AsMissile(void* object) = 0;
	virtual std::shared_ptr<IAttackableUnit> AsAttackableUnit(void* object) = 0;
	virtual std::shared_ptr<IAI> AsAI(void* object) = 0;
	virtual std::shared_ptr<IHero> AsHero(void* object) = 0;
	virtual std::shared_ptr<ITurret> AsTurret(void* object) = 0;
	virtual std::shared_ptr<IMinion> AsMinion(void* object) = 0;

	virtual IGame GetGame() = 0;

	virtual bool IsChatOpen() = 0;
	virtual bool IsConsoleOpen() = 0;

	virtual HWND GetWindow() = 0;
	virtual bool IsWindowInForeground() = 0;

	virtual void PrintChat(std::string format, ...) = 0;
	virtual void ConsoleWrite(std::string format, ...) = 0;
	virtual void ConsoleClear() = 0;

	virtual void EnableInput() = 0;
	virtual void DisableInput() = 0;

	virtual void* GetD3DDevice() = 0;

	virtual float GetTime() = 0;
	virtual float GetTimeTickCount() = 0;
	virtual unsigned GetLatency() = 0;

	virtual std::vector<ISkin> GetHeroSkins(std::string championName) = 0;
	virtual std::vector<std::shared_ptr<IHero>> GetHeroes(bool enemies, bool allies) = 0;
	virtual std::vector<std::shared_ptr<IMinion>> GetMinions(bool enemies, bool allies, bool neutrals) = 0;
	virtual std::vector<std::shared_ptr<ITurret>> GetTurrets(bool enemies, bool allies) = 0;
	virtual std::vector<std::shared_ptr<IAI>> GetInhibitors(bool enemies, bool allies) = 0;
	virtual std::vector<std::shared_ptr<IAI>> GetNexuses(bool enemies, bool allies) = 0;
	virtual std::vector<std::shared_ptr<IObject>> GetObjects() = 0;
	virtual std::vector<std::shared_ptr<IMissile>> GetMissiles() = 0;

	virtual std::shared_ptr<IObject> GetObjectFromId(unsigned id) = 0;
	virtual int GetObjectsCount() = 0;

	virtual std::string BuffTypeToString(unsigned type) = 0;
	virtual std::string AbilityResourceTypeToString(unsigned type) = 0;

	virtual void MoveMouse(SDKVECTOR coordinates) = 0;
	virtual void Ping(SDKVECTOR coordinates, unsigned char type, bool playAudio) = 0;
	virtual void SendChat(std::string format, ...) = 0;
	virtual void ShowEmote(unsigned char emote) = 0;

	virtual bool IsWall(SDKVECTOR coordinates) = 0;

	virtual SDKVECTOR ScreenToWorld(SDKPOINT point) = 0;
	virtual SDKPOINT WorldToScreen(SDKVECTOR point) = 0;
	virtual SDKPOINT WorldToMinimap(SDKVECTOR point) = 0;

	virtual void DrawString(SDKVECTOR* worldPosition, SDKPOINT* screenPosition, std::string text, std::string face, SDKCOLOR color, int height, int width, int weight, bool italic) = 0;
	virtual void DrawLine(SDKVECTOR start, SDKVECTOR end, float width, SDKCOLOR color, int texture) = 0;
	virtual void DrawLine(SDKPOINT start, SDKPOINT end, float width, SDKCOLOR color) = 0;
	virtual void DrawCircle(SDKVECTOR origin, float radius, SDKCOLOR color, int texture, SDKVECTOR direction) = 0;
	virtual void DrawCone(SDKVECTOR origin, float length, float angle, SDKVECTOR direction, SDKCOLOR color, int texture) = 0;

	virtual std::shared_ptr<IHero> GetLocalPlayer() = 0;
};

struct IMenu
{
	virtual ~IMenu() = default;
	virtual void AddBulletPoint() = 0;
	virtual void AddBulletText(std::string format, ...) = 0;
	virtual void AddButton(std::string title, std::function<void()> function) = 0;
	virtual void AddSmallButton(std::string title, std::function<void()> function) = 0;
	virtual void AddWindow(std::string name, std::string title, bool visible, bool collapsed, std::function<void()> function) = 0;
	virtual void AddTree(std::string name, std::string title, bool expanded, std::function<void()> function) = 0;
	virtual void AddCheckbox(std::string name, std::string title, bool value) = 0;
	virtual bool GetBoolean(std::string name) = 0;
	virtual void SetBoolean(std::string name, bool value) = 0;
	virtual void AddInteger(std::string name, std::string title, int value, int min = INT_MIN, int max = INT_MAX) = 0;
	virtual int GetInteger(std::string name) = 0;
	virtual void SetInteger(std::string name, int value) = 0;
	virtual void AddFloat(std::string name, std::string title, float value, int precision = 2, float min = FLT_MIN, float max = FLT_MAX) = 0;
	virtual float GetFloat(std::string name) = 0;
	virtual void AddSelection(std::string name, std::string title, std::vector<std::string> items, int value) = 0;
	virtual int GetSelection(std::string name) = 0;
	virtual void SetSelection(std::string name, int value) = 0;
	virtual void AddColorpicker(std::string name, std::string title, SDKVECTOR value) = 0;
	virtual SDKCOLOR GetColor(std::string name) = 0;
	virtual void ForceOnSameLine() = 0;
	virtual void AddToggle(std::string name, std::string title, bool value, const char* key) = 0;
	virtual void AddKey(std::string name, std::string title, const char* value) = 0;
	virtual int GetKey(std::string name) = 0;
};