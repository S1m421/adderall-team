#include "Include.h"

void Cassiopeia::Create()
{
	Menu::Create();
	Events::Create();
}

long Cassiopeia_lastAttackTime;
long Cassiopeia_getTime()
{
	SYSTEMTIME time; GetSystemTime(&time);
	return time.wSecond * 1000 + time.wMilliseconds;
}

void Cassiopeia_onAIAttack(void* ai, void* target_object, bool start_attack, bool stop_attack, void* user_data)
{
	if (ai == pLocalPlayer->Hero->BaseObject)
	{
		if (!stop_attack) { Cassiopeia_lastAttackTime = Cassiopeia_getTime(); }
		else { Cassiopeia_lastAttackTime = 0; }
	}
}

void Cassiopeia::Combo()
{
	auto QData = pLocalPlayer->Hero->GetSpell(0);
	auto EData = pLocalPlayer->Hero->GetSpell(2);
	auto WData = pLocalPlayer->Hero->GetSpell(1);

	auto Qstate = pLocalPlayer->Hero->CanCastSpell(0);
	auto Wstate = pLocalPlayer->Hero->CanCastSpell(1);
	auto Estate = pLocalPlayer->Hero->CanCastSpell(2);
	auto Rstate = pLocalPlayer->Hero->CanCastSpell(3);

	auto CassioTarget_Q = pTargetSelector->GetTarget(QData.CastRange, DamageType::Magical);
	auto CassioTarget_W = pTargetSelector->GetTarget(pLocalPlayer->Hero->GetSpell(1).CastRange, DamageType::Magical);
	auto CassioTarget_E = pTargetSelector->GetTarget(EData.CastRange, DamageType::Magical);
	auto CassioTarget_R = pTargetSelector->GetTarget(pLocalPlayer->Hero->GetSpell(3).CastRange, DamageType::Magical);
	auto TargetStable = pTargetSelector->GetTarget(1000, DamageType::Magical);

	if (pLocalPlayer->Hero->GetLevel() > 8)
		pOrbwalker->DisableAttack();

	if (TargetStable != nullptr)
	{
		if (pLocalPlayer->Hero->HasItem(3116))
		{
			if ((CassioTarget_E) && (Estate == true) && (pMenu->GetBoolean("Cassiopeia.Combo.E")))
			{
				pLocalPlayer->CastSpell(CassioTarget_E, 2);
			}

			if ((CassioTarget_Q) && (Qstate) && (pMenu->GetBoolean("Cassiopeia.Combo.Q")))
			{
				auto SCQ = Circular(CassioTarget_Q, (QData.CastDelay * 1.30f), (QData.CastRange * 0.97f), (QData.PrimaryCastRadius * 0.95f));
				if (SDKVECTORMATH(SCQ) != SDKVECTORMATH(0, 0, 0))
					pLocalPlayer->CastSpell(SCQ, 0);
			}
		}
		else
		{
			if ((CassioTarget_Q) && (Qstate) && (pMenu->GetBoolean("Cassiopeia.Combo.Q")))
			{
				auto CQ = Circular(CassioTarget_Q, (QData.CastDelay * 1.40f), (QData.CastRange * 0.95f), (QData.PrimaryCastRadius * 0.9f));
				if (SDKVECTORMATH(CQ) != SDKVECTORMATH(0, 0, 0))
					pLocalPlayer->CastSpell(CQ, 0);
			}

			if ((CassioTarget_E) && (Estate == true) && (pMenu->GetBoolean("Cassiopeia.Combo.E")
				&& (pMenu->GetBoolean("Cassiopeia.Combo.Q") && (Qstate == false))))
			{
				pLocalPlayer->CastSpell(CassioTarget_E, 2);
			}

			if ((CassioTarget_E) && (Estate == true) && (pMenu->GetBoolean("Cassiopeia.Combo.E")
				&& (!pMenu->GetBoolean("Cassiopeia.Combo.Q"))))
			{
				pLocalPlayer->CastSpell(CassioTarget_E, 2);
			}
		}

		if ((CassioTarget_W) && (Wstate == true) && (pMenu->GetBoolean("Cassiopeia.Combo.W")))
		{
			auto EnemyPositionForW = CassioTarget_W->GetPosition();
			auto PlayerPosition = pLocalPlayer->Hero->GetPosition();
			float DistanceBetweenTargetWandPlayer = SDKVECTORMATH(PlayerPosition).Distance(SDKVECTORMATH(EnemyPositionForW));
			auto CW = Circular(CassioTarget_W, (WData.CastDelay * 1.40f), (WData.CastRange * 0.90f), (WData.PrimaryCastRadius * 0.90f));

			if ((DistanceBetweenTargetWandPlayer > 510) && (DistanceBetweenTargetWandPlayer < 790))
			{
				if ((CassioTarget_W->IsSlowed()) || (CassioTarget_W->IsStunned()) || (CassioTarget_W->IsAttacking()))
				{
					if (SDKVECTORMATH(CW) != SDKVECTORMATH(0, 0, 0))
						pLocalPlayer->CastSpell(CW, 1);
				}
			}
		}
	}
}

void Cassiopeia::Mixed()
{
	auto QData = pLocalPlayer->Hero->GetSpell(0);
	auto EData = pLocalPlayer->Hero->GetSpell(2);

	auto Qstate = pLocalPlayer->Hero->CanCastSpell(0);
	auto Estate = pLocalPlayer->Hero->CanCastSpell(2);

	auto CassioTarget_Q = pTargetSelector->GetTarget(QData.CastRange, DamageType::Magical);
	auto CassioTarget_E = pTargetSelector->GetTarget(EData.CastRange, DamageType::Magical);

	pOrbwalker->EnableAttack();

	if (CassioTarget_Q != nullptr)
	{
		auto CQ = Circular(CassioTarget_Q, (QData.CastDelay * 1.40f), (QData.CastRange * 0.95f), (QData.PrimaryCastRadius * 0.9f));
		auto SCQ = Circular(CassioTarget_Q, (QData.CastDelay * 1.30f), (QData.CastRange * 0.97f), (QData.PrimaryCastRadius * 0.95f));

		if ((Qstate) && (pMenu->GetBoolean("Cassiopeia.Harass.Q")))
		{
			if (pMenu->GetBoolean("Cassiopeia.Harass.QS"))
			{
				if (CassioTarget_Q->IsAttacking() || CassioTarget_Q->IsSlowed() || CassioTarget_Q->IsStunned() || CassioTarget_Q->IsTaunted())
				{
					if (SDKVECTORMATH(SCQ) != SDKVECTORMATH(0, 0, 0))
						SdkCastSpellLocalPlayer(NULL, &SCQ, 0, 0);
				}
			}
			else if (!pMenu->GetBoolean("Cassiopeia.Harass.Q"))
			{
				if (SDKVECTORMATH(CQ) != SDKVECTORMATH(0, 0, 0))
					SdkCastSpellLocalPlayer(NULL, &CQ, 0, 0);
			}
		}
		
		if ((CassioTarget_E != nullptr) && (pMenu->GetBoolean("Cassiopeia.Harass.E")))
		{
			if (pMenu->GetBoolean("Cassiopeia.Harass.Q"))
			{
				if (Estate == true && Qstate == false)
						pLocalPlayer->CastSpell(CassioTarget_E, 2);
			}
			else if (!pMenu->GetBoolean("Cassiopeia.Harass.Q"))
			{
				if (Estate == true)
						pLocalPlayer->CastSpell(CassioTarget_E, 2);
			}
		}
	}
}

void Cassiopeia::Lasthit()
{
	auto QData = pLocalPlayer->Hero->GetSpell(0);
	auto EData = pLocalPlayer->Hero->GetSpell(2);
	auto Qstate = pLocalPlayer->Hero->CanCastSpell(0);
	auto Wstate = pLocalPlayer->Hero->CanCastSpell(1);
	auto Estate = pLocalPlayer->Hero->CanCastSpell(2);
	auto CassioTarget_E = pTargetSelector->GetTarget((EData.CastRange), DamageType::Magical);
	
	if (pMenu->GetBoolean("Cassiopeia.LastHit.E"))
	{
		if ((Estate == true) && (pMenu->GetBoolean("Cassiopeia.LastHit.AA")))
		{
			pOrbwalker->DisableAttack();
		}
		else
		{
			pOrbwalker->EnableAttack();
		}

		for (auto const& minion : pSDK->GetMinions(true, false, false))
		{
			auto MinionHP = minion->GetHealth();

			if ((MinionHP.Current) <= (MinionHP.Max * 0.6f))
			{
				SDKCOLOR ColorPurple = { 128, 0, 128, 255 };
				SDKVECTOR DirectionVectorLH = { 0, 0, 1.f };
				auto MinionPosition = minion->GetPosition();
				auto PlayerPosition = pLocalPlayer->Hero->GetPosition();
				float DistanceBetweenMinionAndPlayer = SDKVECTORMATH(PlayerPosition).Distance(SDKVECTORMATH(MinionPosition));

				if (DistanceBetweenMinionAndPlayer < EData.CastRange)
				{
					const auto spellLevel = pLocalPlayer->Hero->GetSpell(2).Level - 1;
					std::vector<int> baseDamage = { 9, 29, 49, 69, 89 };
					auto pDamage = 0.f; auto mDamage = 0.f; auto tDamage = 0.f;
					auto attackCastDelay = pLocalPlayer->Hero->GetAttackCastDelay();
					mDamage = (baseDamage[spellLevel]) + (pLocalPlayer->Hero->GetAbilityPower() * 0.1f);

					if (((MinionHP.Current + MinionHP.MagicalShield) < (mDamage)) && ((MinionHP.Current) > (MinionHP.Max * 0.075)))
					{
							SdkCastSpellLocalPlayer(minion->BaseObject, NULL, 2, 0);
					}
				}
			}
		}
	}
}

void Cassiopeia::Clear()
{
	auto QData = pLocalPlayer->Hero->GetSpell(0);
	auto EData = pLocalPlayer->Hero->GetSpell(2);
	auto Qstate = pLocalPlayer->Hero->CanCastSpell(0);
	auto Wstate = pLocalPlayer->Hero->CanCastSpell(1);
	auto Estate = pLocalPlayer->Hero->CanCastSpell(2);
	auto CassioTarget_E = pTargetSelector->GetTarget((EData.CastRange), DamageType::Magical);

	pOrbwalker->EnableAttack();

	if (pMenu->GetBoolean("Cassiopeia.Clear.Wave"))
	{
		for (auto const& minion : pSDK->GetMinions(true, false, false))
		{
			auto MinionHP = minion->GetHealth();
			SDKCOLOR ColorPurple = { 128, 0, 128, 255 };
			SDKVECTOR DirectionVectorLH = { 0, 0, 1.f };
			auto MinionPosition = minion->GetPosition();
			auto PlayerPosition = pLocalPlayer->Hero->GetPosition();
			float DistanceBetweenMinionAndPlayer = SDKVECTORMATH(PlayerPosition).Distance(SDKVECTORMATH(MinionPosition));

			if (DistanceBetweenMinionAndPlayer < EData.CastRange)
			{
				const auto spellLevel = pLocalPlayer->Hero->GetSpell(2).Level - 1;
				std::vector<int> baseDamage = { 9, 29, 49, 69, 89 };
				auto pDamage = 0.f; auto mDamage = 0.f; auto tDamage = 0.f;
				auto attackCastDelay = pLocalPlayer->Hero->GetAttackCastDelay();
				mDamage = (baseDamage[spellLevel]) + (pLocalPlayer->Hero->GetAbilityPower() * 0.1f);

				bool isAfterAA = Cassiopeia_lastAttackTime != 0 && Cassiopeia_getTime() - Cassiopeia_lastAttackTime > attackCastDelay * 1000.0f;
				if (isAfterAA) { Cassiopeia_lastAttackTime = 0; }

				if (((MinionHP.Current + MinionHP.MagicalShield) < (mDamage)) && Estate == true)
				{
					SdkCastSpellLocalPlayer(minion->BaseObject, NULL, 2, 0);
				}
				else if ((MinionHP.Current + MinionHP.MagicalShield) > (mDamage * 2.5) && (Estate == true))
				{
					SdkCastSpellLocalPlayer(minion->BaseObject, NULL, 2, 0);
				}
			}
		}
	}

	if (pMenu->GetBoolean("Cassiopeia.Clear.Jungle"))
	{
		for (auto const& minion : pSDK->GetMinions(false, false, true))
		{
			auto MinionHP = minion->GetHealth();
			SDKCOLOR ColorPurple = { 128, 0, 128, 255 };
			SDKVECTOR DirectionVectorLH = { 0, 0, 1.f };
			auto MinionPosition = minion->GetPosition();
			auto PlayerPosition = pLocalPlayer->Hero->GetPosition();
			float DistanceBetweenMinionAndPlayer = SDKVECTORMATH(PlayerPosition).Distance(SDKVECTORMATH(MinionPosition));

			if (DistanceBetweenMinionAndPlayer < EData.CastRange)
			{
				const auto spellLevel = pLocalPlayer->Hero->GetSpell(2).Level - 1;
				std::vector<int> baseDamage = { 9, 29, 49, 69, 89 };
				auto pDamage = 0.f; auto mDamage = 0.f; auto tDamage = 0.f;
				auto attackCastDelay = pLocalPlayer->Hero->GetAttackCastDelay();

				bool isAfterAA = Cassiopeia_lastAttackTime != 0 && Cassiopeia_getTime() - Cassiopeia_lastAttackTime > attackCastDelay * 1000.0f;
				if (isAfterAA) { Cassiopeia_lastAttackTime = 0; }

				if (Qstate)
					SdkCastSpellLocalPlayer(minion->BaseObject, NULL, 0, 0);

				if (Estate)
					SdkCastSpellLocalPlayer(minion->BaseObject, NULL, 2, 0);
			}
		}
	}
}