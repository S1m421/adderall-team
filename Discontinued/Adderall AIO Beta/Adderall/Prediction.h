#pragma once

#include "sdkapi.h"
#include "d3dx9.h"
#include "Vectormath.h"



extern PSDK_CONTEXT SDK_CONTEXT_GLOBAL;

extern std::shared_ptr<ISDK> pSDK;
extern std::shared_ptr<ILocalPlayer> pLocalPlayer;


inline SDKVECTOR __cdecl Circular(_In_ std::shared_ptr<IHero> Target, _In_ float castTime, _In_ float range, _In_ float radius) {
	    SDKVECTOR emptyVec = { 0, 0, 0 };
	    INavData tNav = Target->GetNavData(); 

	    if (tNav.Waypoints&&tNav.NumberOfWaypoints) {
			PSDKVECTOR Waypoint = &tNav.Waypoints[tNav.NextWaypoint]; 
			SDKVECTORMATH orientation(tNav.Velocity); orientation.Normalize(); 
			SDKVECTORMATH result = SDKVECTORMATH(Target->GetPosition()) + Target->GetMovementSpeed()*orientation*castTime;
		
			bool isWall;
			SdkIsLocationWall(&result.toSdk(), &isWall); 
		
			if (isWall) {
				if (SDKVECTORMATH(pLocalPlayer->Hero->GetPosition()).Distance(*Waypoint) > range) { return emptyVec; }
				else { return*Waypoint; }
			}
			float waydist = SDKVECTORMATH(Target->GetPosition()).Distance(*Waypoint); 
			float preddist = SDKVECTORMATH(Target->GetPosition()).Distance(result); if (preddist > waydist) {
				if (SDKVECTORMATH(pLocalPlayer->Hero->GetPosition()).Distance(*Waypoint) > range) { return emptyVec; }
				else { return*Waypoint; }
			}
			if (SDKVECTORMATH(pLocalPlayer->Hero->GetPosition()).Distance(result) > range)
				return emptyVec; return result.toSdk();
	    }
	    else {
			if (SDKVECTORMATH(pLocalPlayer->Hero->GetPosition()).Distance(Target->GetPosition()) > range) { 
				return emptyVec; 
			}
			else { return SDKVECTORMATH(Target->GetPosition()).toSdk(); }
		}
}



inline SDKVECTOR __cdecl Linear(_In_ std::shared_ptr<IHero> Target, _In_ float castTime, _In_ float missileSpeed, _In_ float range, _In_ float width) {
	//set empty vec
	SDKVECTOR emptyVec = { 0, 0, 0 };
	// Get Target NavData
	INavData tNav = Target->GetNavData();
	// If has Waypoints we proceed
	if (tNav.Waypoints && tNav.NumberOfWaypoints) {
		// Get Next Waypoints
		PSDKVECTOR Waypoint = &tNav.Waypoints[tNav.NextWaypoint];

		//check ping
		//unsigned int myPing;
		//SdkGetNetPing(&myPing);	
		//float inputDelay = myPing / 2000.f;

		// line waypoint
		//SDKCOLOR ColorRed = { 0, 0, 255, 255 };
		//SdkDrawLine(&Target->GetPosition(), Waypoint, 5.f, &ColorRed, 0);
		

		// Calculate distance btwen LocalPlayer and Target, divide by missile speed and add the castTime
		float ata = (SDKVECTORMATH(pLocalPlayer->Hero->GetPosition()).Distance(Target->GetPosition()) / missileSpeed) + castTime;
		// Normalize Target velocity
		SDKVECTORMATH orientation(tNav.Velocity); orientation.Normalize();

		// Calculate where the target will be in the estimated time
		SDKVECTORMATH result = SDKVECTORMATH(Target->GetPosition()) + Target->GetMovementSpeed() * orientation * ata; /* (inputDelay / 1000.f*/

		bool isWall;
		// Check if result is inside wall, if it is, just return last Waypoint
		SdkIsLocationWall(&result.toSdk(), &isWall);
		if (isWall) {
			if (SDKVECTORMATH(pLocalPlayer->Hero->GetPosition()).Distance(*Waypoint) > range) {
				return emptyVec;
			}
			else {
				return *Waypoint;
			}
		}
		// Calculate if distante to waypoint is bigger then distance to prediction
		float waydist = SDKVECTORMATH(Target->GetPosition()).Distance(*Waypoint);
		float preddist = SDKVECTORMATH(Target->GetPosition()).Distance(result);
		// If prediction distante is bigger, return the waypoint
		if (preddist > waydist) {
			if (SDKVECTORMATH(pLocalPlayer->Hero->GetPosition()).Distance(*Waypoint) > range) {
				return emptyVec;
			}
			else {
				return *Waypoint;
			}
		}
		// If not, return prediction
		if (SDKVECTORMATH(pLocalPlayer->Hero->GetPosition()).Distance(result) > range)
			return emptyVec;

		return result.toSdk();
	}
	else {
		// If target isnt moving, return his current position
		if (SDKVECTORMATH(pLocalPlayer->Hero->GetPosition()).Distance(Target->GetPosition()) > range) {
			return emptyVec;
		}
		else {
			return SDKVECTORMATH(Target->GetPosition()).toSdk();
		}
	}
}

inline bool PointOnLineSegment(D3DXVECTOR2 pt1, D3DXVECTOR2 pt2, D3DXVECTOR2 pt, double epsilon = 0.001) {
	if (pt.x - std::fmax(pt1.x, pt2.x) > epsilon ||
		std::fmin(pt1.x, pt2.x) - pt.x > epsilon ||
		pt.y - std::fmax(pt1.y, pt2.y) > epsilon ||
		std::fmin(pt1.y, pt2.y) - pt.y > epsilon)
		return false;
	if (abs(pt2.x - pt1.x) < epsilon)
		return abs(pt1.x - pt.x) < epsilon || abs(pt2.x - pt.x) < epsilon;
	if (abs(pt2.y - pt1.y) < epsilon)
		return abs(pt1.y - pt.y) < epsilon || abs(pt2.y - pt.y) < epsilon;
	double x = pt1.x + (pt.y - pt1.y) * (pt2.x - pt1.x) / (pt2.y - pt1.y);
	double y = pt1.y + (pt.x - pt1.x) * (pt2.y - pt1.y) / (pt2.x - pt1.x);
	return abs(pt.x - x) < epsilon || abs(pt.y - y) < epsilon;
}

inline bool Collision(SDKVECTORMATH vec, float radius) {
	for (auto const& minion : pSDK->GetMinions(true, false, true))
	{

		auto Position = minion->GetPosition();

		if (PointOnLineSegment(
			D3DXVECTOR2(pLocalPlayer->Hero->GetPosition().x, pLocalPlayer->Hero->GetPosition().z),
			D3DXVECTOR2(vec.x, vec.z),
			D3DXVECTOR2(Position.x, Position.z), (radius * 1.5f))) 
		{
			return true;
		}
	}
	return false;
}