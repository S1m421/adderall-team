#pragma once
namespace Events
{
	void Create();
	void Remove();
	void GameUpdate(void* userData);

	static int GameUpdateId = 0;
};
