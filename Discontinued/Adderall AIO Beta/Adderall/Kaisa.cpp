#include "Include.h"


float getWDamage(std::shared_ptr<IAI> enemy)
{
	const auto spellLevel = pLocalPlayer->Hero->GetSpell(1).Level - 1;
	std::vector<int> baseDamage = { 20, 45, 70, 95, 120 };
	
	auto pDamage = 0.f; auto mDamage = 0.f; auto tDamage = 0.f;

	mDamage = baseDamage[spellLevel] + pLocalPlayer->Hero->GetAbilityPower() * 0.60f;
	pDamage = pLocalPlayer->Hero->GetAttackDamage() * 1.50f;
	tDamage = pLocalPlayer->Hero->GetPhysicalDamage(enemy, pDamage) + pLocalPlayer->Hero->GetMagicDamage(enemy, mDamage);
		return tDamage;
}

void Kaisa::Create()
{
	Menu::Create();
	Events::Create();
}

void Kaisa::Always()
{
	if (pLocalPlayer->Hero->CanCastSpell(1) && (pMenu->GetBoolean("1337.Combo.WKS")))
	{
		auto target = pTargetSelector->GetTarget(3000, DamageType::Magical);

		if (target != nullptr && target->IsDead() == false) {
			if (getWDamage(target) >= (target->GetHealth().Current)) {						
                auto W = pLocalPlayer->Hero->GetSpell(1);
                auto KaisaW = Linear(target, W.CastDelay, W.MissileSpeed, W.CastRange, W.LineWidth);
                if (SDKVECTORMATH(KaisaW) != SDKVECTORMATH(0, 0, 0))
                    if (!Collision(KaisaW, W.LineWidth))
                        pLocalPlayer->CastSpell(KaisaW, 1);
            }
					    
        }
	}
}

void Kaisa::Combo()
{
	if (pLocalPlayer->Hero->CanCastSpell(0) && (pMenu->GetBoolean("1337.Combo.Q")))
	{
		auto target = pTargetSelector->GetTarget(600, DamageType::Physical);
		if (target != nullptr) {
			pLocalPlayer->CastSpell(0);
		}
	}

	if (pLocalPlayer->Hero->CanCastSpell(1) && (pMenu->GetBoolean("1337.Combo.W")))
	{
		auto target = pTargetSelector->GetTarget(3000, DamageType::Magical);
		auto minions = pSDK->GetMinions(true, false, false);
		
		if (target != nullptr) {
            auto W = pLocalPlayer->Hero->GetSpell(1);
			auto KaisaW = Linear(target, W.CastDelay, W.MissileSpeed, W.CastRange, W.LineWidth);
			if (SDKVECTORMATH(KaisaW) != SDKVECTORMATH(0, 0, 0))
				if (!Collision(KaisaW, W.LineWidth))
					pLocalPlayer->CastSpell(KaisaW, 1);
		}
	}
}

void Kaisa::Lasthit() { }
void Kaisa::Mixed() { }
void Kaisa::Clear() { }
void Kaisa::Flee() { }
